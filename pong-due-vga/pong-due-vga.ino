#include "src/VGA/VGA.h"
 
#define NEGRO    0x00
#define ROJO     0xE0
#define VERDE    0x1C
#define AZUL     0x03
#define AMARILLO ROJO + VERDE
#define MAGENTA  ROJO + AZUL
#define CYAN     VERDE + AZUL
#define BLANCO   ROJO + VERDE + AZUL

const byte CP1 = ROJO;
const byte CP2 = AZUL;
const byte CB =  VERDE;
const byte CN =  BLANCO;
const byte CW =  AMARILLO;
const byte CL =  MAGENTA;

const int puntajeTerminacion = 3;

const int alturaPaddle = 80;
const int anchuraPaddle = 16;
const int tamBola = 16;

int posP1 = (240 - alturaPaddle) / 2;
int velP1 = 0;
int posP2 = (240 - alturaPaddle) / 2;
int velP2 = 0;

int posBolaX = (320 - tamBola) / 2;
int posBolaY = (240 - tamBola) / 2;
int velBolaX = 0;
int velBolaY = 0;

int scoreP1 = 0;
int scoreP2 = 0;
int digScoreP1 = 0;
int digScoreP2 = 0;

bool arrancarJuego = false;

int cuentaRegresiva;
int conteoTiempo;
int tInicio;

typedef enum {
  EJ_INICIO, EJ_ARRANQUE, EJ_CONTIENDA, EJ_PUNTO_P1, EJ_PUNTO_P2,
  EJ_FIN,
} ESTADO_JUEGO;

ESTADO_JUEGO estadoJuego = EJ_INICIO;

void setup() {
  Serial.begin(9600);

  //Inicializa la libreria VGA
  VGA.begin(320,240,VGA_COLOUR);
}

void loop() {
  leerControles();
  VGA.waitSync();
  borrarEscena();
  logicaJuego();
  dibujarEscena();
}

void leerControles() {
  char dato;

  if (!Serial.available()) return;

  dato = Serial.read();

  switch(dato) {
    case 'a':
    case 'b':
      velP1 = 0;  break;
    case 'A':
      velP1 = -2; break;
    case 'B':
      velP1 = 2;  break;
    case 'c':
    case 'd':
      velP2 = 0;  break;
    case 'C':
      velP2 = -2; break;
    case 'D':
      velP2 = 2;  break;
    case 'E':
      arrancarJuego = true; break;
  }
}

void logicaJuego() {
  switch (estadoJuego) {
    case EJ_INICIO:
      posBolaX = (320 - tamBola) / 2;
      posBolaY = (240 - tamBola) / 2;
      posP1 = (240 - alturaPaddle) / 2;
      posP2 = (240 - alturaPaddle) / 2;
      digScoreP1 = 0;
      digScoreP2 = 0;
      scoreP1 = 0;
      scoreP2 = 0;
      if (arrancarJuego) {
        arrancarJuego = false;
        tInicio = millis();
        estadoJuego = EJ_ARRANQUE;
      }

      break;
    case EJ_ARRANQUE:
      posP1 = (240 - alturaPaddle) / 2;
      posP2 = (240 - alturaPaddle) / 2;
      posBolaX = (320 - tamBola) / 2;
      posBolaY = (240 - tamBola) / 2;

      cuentaRegresiva = (tInicio + 3999 - millis()) / 1000;

      if (cuentaRegresiva <= 0) {
        randomSeed(millis());
        if (random(2))
          velBolaX = 1;
        else
          velBolaX = -1;
  
        switch (random(3)) {
          case 0:
            velBolaY = 0;
            break;
          case 1:
            velBolaY = 1;
            break;
          case 2:
            velBolaY = -1;
            break;
        }

        borrarCuentaRegresiva();
        estadoJuego = EJ_CONTIENDA;
      }
      else {
        dibujarCuentaRegresiva();
      }

      if (arrancarJuego) {
        arrancarJuego = false;
        borrarCuentaRegresiva();
        estadoJuego = EJ_INICIO;
      }

      break;
    case EJ_CONTIENDA:
      posBolaX += velBolaX;
      posBolaY += velBolaY;

      //Choque con borde derecho
      if (posBolaX + tamBola >= 320) {
        scoreP1++;
        tInicio = millis();
        estadoJuego = EJ_PUNTO_P1;
      }

      //Choque con borde izquierdo
      if (posBolaX <= 0) {
        scoreP2++;
        tInicio = millis();
        estadoJuego = EJ_PUNTO_P2;
      }

      //Choque con paddle izquierdo
      if (posBolaX == anchuraPaddle &&
          posBolaY >= posP1 - tamBola + 1 &&
          posBolaY < posP1 + alturaPaddle)
      {
        velBolaX = 1;

        if (posBolaY + tamBola / 2 <
            posP1 + alturaPaddle / 3)
        {
          velBolaY = -1;
        }
        else if (posBolaY + tamBola / 2 >=
                 posP1 + alturaPaddle * 2 / 3)
        {
          velBolaY = 1;
        }
        else
          velBolaY = 0;
      }

      //Choque con paddle derecho
      if (posBolaX == 320 - anchuraPaddle - tamBola &&
          posBolaY >= posP2 - tamBola + 1 &&
          posBolaY < posP2 + alturaPaddle)
      {
        velBolaX = -1;

        if (posBolaY + tamBola / 2 <
            posP2 + alturaPaddle / 3)
        {
          velBolaY = -1;
        }
        else if (posBolaY + tamBola / 2 >=
                 posP2 + alturaPaddle * 2 / 3)
        {
          velBolaY = 1;
        }
        else
          velBolaY = 0;
      }

      //Choque con borde inferior
      if (posBolaY + tamBola >= 240)
        { posBolaY = 240 - tamBola; velBolaY = -1; }

      //Choque con borde superior
      if (posBolaY <= 0)
        { posBolaY = 0; velBolaY = 1; }

      posP1 += velP1;
      if (posP1 < 0) posP1 = 0;
      if (posP1 > 240 - alturaPaddle) posP1 = 240 - alturaPaddle;

      posP2 += velP2;
      if (posP2 < 0) posP2 = 0;
      if (posP2 > 240 - alturaPaddle) posP2 = 240 - alturaPaddle;

      if (arrancarJuego) {
        arrancarJuego = false;
        estadoJuego = EJ_INICIO;
      }

      break;
    case EJ_PUNTO_P1:
      conteoTiempo = millis() - tInicio;
      if ((conteoTiempo / 500) % 2)
        digScoreP1 = 10;
      else
        digScoreP1 = scoreP1;

      if (conteoTiempo >= 3000) {
        tInicio = millis();
        if (scoreP1 < puntajeTerminacion)
          estadoJuego = EJ_ARRANQUE;
        else {
          dibujarWinLose(scoreP2 > scoreP1);
          estadoJuego = EJ_FIN;
        }
      }

      if (arrancarJuego) {
        arrancarJuego = false;
        estadoJuego = EJ_INICIO;
      }

      break;
    case EJ_PUNTO_P2:
      conteoTiempo = millis() - tInicio;
      if ((conteoTiempo / 500) % 2)
        digScoreP2 = 10;
      else
        digScoreP2 = scoreP2;

      if (conteoTiempo >= 3000) {
        tInicio = millis();
        if (scoreP2 < puntajeTerminacion)
          estadoJuego = EJ_ARRANQUE;
        else {
          dibujarWinLose(scoreP2 > scoreP1);
          estadoJuego = EJ_FIN;
        }
      }

      if (arrancarJuego) {
        arrancarJuego = false;
        estadoJuego = EJ_INICIO;
      }

      break;
    case EJ_FIN:
      conteoTiempo = millis() - tInicio;
      if (conteoTiempo >= 10000 || arrancarJuego) {
        borrarWinLose(scoreP2 > scoreP1);
        estadoJuego = EJ_INICIO;
      }

      break;
  }
}
