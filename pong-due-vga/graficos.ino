const byte digito[3 * 5 * 11] = {
  CN, CN, CN,
  CN,  0, CN,
  CN,  0, CN,
  CN,  0, CN,
  CN, CN, CN,

   0,  0, CN,
   0,  0, CN,
   0,  0, CN,
   0,  0, CN,
   0,  0, CN,

  CN, CN, CN,
   0,  0, CN,
  CN, CN, CN,
  CN,  0,  0,
  CN, CN, CN,

  CN, CN, CN,
   0,  0, CN,
  CN, CN, CN,
   0,  0, CN,
  CN, CN, CN,

  CN,  0, CN,
  CN,  0, CN,
  CN, CN, CN,
   0,  0, CN,
   0,  0, CN,

  CN, CN, CN,
  CN,  0,  0,
  CN, CN, CN,
   0,  0, CN,
  CN, CN, CN,

  CN, CN, CN,
  CN,  0,  0,
  CN, CN, CN,
  CN,  0, CN,
  CN, CN, CN,

  CN, CN, CN,
   0,  0, CN,
   0,  0, CN,
   0,  0, CN,
   0,  0, CN,

  CN, CN, CN,
  CN,  0, CN,
  CN, CN, CN,
  CN,  0, CN,
  CN, CN, CN,

  CN, CN, CN,
  CN,  0, CN,
  CN, CN, CN,
   0,  0, CN,
  CN, CN, CN,

   0,  0,  0,
   0,  0,  0,
   0,  0,  0,
   0,  0,  0,
   0,  0,  0,
};

const byte msgWin[12 * 5] {
  CW,  0, CW,  0, CW,  0, CW,  0, CW,  0,  0, CW,
  CW,  0, CW,  0, CW,  0, CW,  0, CW, CW,  0, CW,
  CW,  0, CW,  0, CW,  0, CW,  0, CW,  0, CW, CW,
  CW,  0, CW,  0, CW,  0, CW,  0, CW,  0, CW, CW,
   0, CW,  0, CW,  0,  0, CW,  0, CW,  0,  0, CW,
};

const byte msgLose[15 * 5] {
  CL,  0,  0,  0, CL, CL, CL,  0, CL, CL, CL,  0, CL, CL, CL,
  CL,  0,  0,  0, CL,  0, CL,  0, CL,  0,  0,  0, CL,  0,  0,
  CL,  0,  0,  0, CL,  0, CL,  0, CL, CL, CL,  0, CL, CL, CL,
  CL,  0,  0,  0, CL,  0, CL,  0,  0,  0, CL,  0, CL,  0,  0,
  CL, CL, CL,  0, CL, CL, CL,  0, CL, CL, CL,  0, CL, CL, CL,
};

void dibujarEscena() {
  int i, j;
  int pos1, pos2, posD;
  byte pix;

  //Dibuja los paddles
  pos1 = 320 * posP1;
  pos2 = 320 * posP2 + 320 - anchuraPaddle;
  for (i=0; i<alturaPaddle; i++) {
    VGA.cb[pos1 +  0] = CP1;
    VGA.cb[pos1 +  1] = CP1;
    VGA.cb[pos1 +  2] = CP1;
    VGA.cb[pos1 +  3] = CP1;
    VGA.cb[pos1 +  4] = CP1;
    VGA.cb[pos1 +  5] = CP1;
    VGA.cb[pos1 +  6] = CP1;
    VGA.cb[pos1 +  7] = CP1;
    VGA.cb[pos1 +  8] = CP1;
    VGA.cb[pos1 +  9] = CP1;
    VGA.cb[pos1 + 10] = CP1;
    VGA.cb[pos1 + 11] = CP1;
    VGA.cb[pos1 + 12] = CP1;
    VGA.cb[pos1 + 13] = CP1;
    VGA.cb[pos1 + 14] = CP1;
    VGA.cb[pos1 + 15] = CP1;

    VGA.cb[pos2 +  0] = CP2;
    VGA.cb[pos2 +  1] = CP2;
    VGA.cb[pos2 +  2] = CP2;
    VGA.cb[pos2 +  3] = CP2;
    VGA.cb[pos2 +  4] = CP2;
    VGA.cb[pos2 +  5] = CP2;
    VGA.cb[pos2 +  6] = CP2;
    VGA.cb[pos2 +  7] = CP2;
    VGA.cb[pos2 +  8] = CP2;
    VGA.cb[pos2 +  9] = CP2;
    VGA.cb[pos2 + 10] = CP2;
    VGA.cb[pos2 + 11] = CP2;
    VGA.cb[pos2 + 12] = CP2;
    VGA.cb[pos2 + 13] = CP2;
    VGA.cb[pos2 + 14] = CP2;
    VGA.cb[pos2 + 15] = CP2;

    pos1 += 320;
    pos2 += 320;
  }

  //Dibuja la bola
  pos1 = 320 * posBolaY + posBolaX;
  for (i=0; i<tamBola; i++) {
    VGA.cb[pos1 +  0] = CB;
    VGA.cb[pos1 +  1] = CB;
    VGA.cb[pos1 +  2] = CB;
    VGA.cb[pos1 +  3] = CB;
    VGA.cb[pos1 +  4] = CB;
    VGA.cb[pos1 +  5] = CB;
    VGA.cb[pos1 +  6] = CB;
    VGA.cb[pos1 +  7] = CB;
    VGA.cb[pos1 +  8] = CB;
    VGA.cb[pos1 +  9] = CB;
    VGA.cb[pos1 + 10] = CB;
    VGA.cb[pos1 + 11] = CB;
    VGA.cb[pos1 + 12] = CB;
    VGA.cb[pos1 + 13] = CB;
    VGA.cb[pos1 + 14] = CB;
    VGA.cb[pos1 + 15] = CB;

    pos1 += 320;
  }

  //Dibuja la puntuacion
  for (i=0; i<2; i++) {
    if (i == 0) {
      pos1 = 320 / 2 - 8 * 4;
      posD = digScoreP1 * 3 * 5;
    }
    else {
      pos1 = 320 / 2 + 4 * 4;
      posD = digScoreP2 * 3 * 5;
    }

    for (j=0; j<5; j++) {
      pix = digito[posD + 0];
      VGA.cb[pos1 +  0 + 320 * 0] = pix;
      VGA.cb[pos1 +  1 + 320 * 0] = pix;
      VGA.cb[pos1 +  2 + 320 * 0] = pix;
      VGA.cb[pos1 +  3 + 320 * 0] = pix;
      VGA.cb[pos1 +  0 + 320 * 1] = pix;
      VGA.cb[pos1 +  1 + 320 * 1] = pix;
      VGA.cb[pos1 +  2 + 320 * 1] = pix;
      VGA.cb[pos1 +  3 + 320 * 1] = pix;
      VGA.cb[pos1 +  0 + 320 * 2] = pix;
      VGA.cb[pos1 +  1 + 320 * 2] = pix;
      VGA.cb[pos1 +  2 + 320 * 2] = pix;
      VGA.cb[pos1 +  3 + 320 * 2] = pix;
      VGA.cb[pos1 +  0 + 320 * 3] = pix;
      VGA.cb[pos1 +  1 + 320 * 3] = pix;
      VGA.cb[pos1 +  2 + 320 * 3] = pix;
      VGA.cb[pos1 +  3 + 320 * 3] = pix;

      pix = digito[posD + 1];
      VGA.cb[pos1 +  4 + 320 * 0] = pix;
      VGA.cb[pos1 +  5 + 320 * 0] = pix;
      VGA.cb[pos1 +  6 + 320 * 0] = pix;
      VGA.cb[pos1 +  7 + 320 * 0] = pix;
      VGA.cb[pos1 +  4 + 320 * 1] = pix;
      VGA.cb[pos1 +  5 + 320 * 1] = pix;
      VGA.cb[pos1 +  6 + 320 * 1] = pix;
      VGA.cb[pos1 +  7 + 320 * 1] = pix;
      VGA.cb[pos1 +  4 + 320 * 2] = pix;
      VGA.cb[pos1 +  5 + 320 * 2] = pix;
      VGA.cb[pos1 +  6 + 320 * 2] = pix;
      VGA.cb[pos1 +  7 + 320 * 2] = pix;
      VGA.cb[pos1 +  4 + 320 * 3] = pix;
      VGA.cb[pos1 +  5 + 320 * 3] = pix;
      VGA.cb[pos1 +  6 + 320 * 3] = pix;
      VGA.cb[pos1 +  7 + 320 * 3] = pix;

      pix = digito[posD + 2];
      VGA.cb[pos1 +  8 + 320 * 0] = pix;
      VGA.cb[pos1 +  9 + 320 * 0] = pix;
      VGA.cb[pos1 + 10 + 320 * 0] = pix;
      VGA.cb[pos1 + 11 + 320 * 0] = pix;
      VGA.cb[pos1 +  8 + 320 * 1] = pix;
      VGA.cb[pos1 +  9 + 320 * 1] = pix;
      VGA.cb[pos1 + 10 + 320 * 1] = pix;
      VGA.cb[pos1 + 11 + 320 * 1] = pix;
      VGA.cb[pos1 +  8 + 320 * 2] = pix;
      VGA.cb[pos1 +  9 + 320 * 2] = pix;
      VGA.cb[pos1 + 10 + 320 * 2] = pix;
      VGA.cb[pos1 + 11 + 320 * 2] = pix;
      VGA.cb[pos1 +  8 + 320 * 3] = pix;
      VGA.cb[pos1 +  9 + 320 * 3] = pix;
      VGA.cb[pos1 + 10 + 320 * 3] = pix;
      VGA.cb[pos1 + 11 + 320 * 3] = pix;

      pos1 += 320 * 4;
      posD += 3;
    }
  }
}

void borrarEscena() {
  int i;
  int pos1, pos2;

  pos1 = 320 * posP1;
  pos2 = 320 * posP2 + 320 - anchuraPaddle;
  for (i=0; i<alturaPaddle; i++) {
    VGA.cb[pos1 +  0] = 0;
    VGA.cb[pos1 +  1] = 0;
    VGA.cb[pos1 +  2] = 0;
    VGA.cb[pos1 +  3] = 0;
    VGA.cb[pos1 +  4] = 0;
    VGA.cb[pos1 +  5] = 0;
    VGA.cb[pos1 +  6] = 0;
    VGA.cb[pos1 +  7] = 0;
    VGA.cb[pos1 +  8] = 0;
    VGA.cb[pos1 +  9] = 0;
    VGA.cb[pos1 + 10] = 0;
    VGA.cb[pos1 + 11] = 0;
    VGA.cb[pos1 + 12] = 0;
    VGA.cb[pos1 + 13] = 0;
    VGA.cb[pos1 + 14] = 0;
    VGA.cb[pos1 + 15] = 0;

    VGA.cb[pos2 +  0] = 0;
    VGA.cb[pos2 +  1] = 0;
    VGA.cb[pos2 +  2] = 0;
    VGA.cb[pos2 +  3] = 0;
    VGA.cb[pos2 +  4] = 0;
    VGA.cb[pos2 +  5] = 0;
    VGA.cb[pos2 +  6] = 0;
    VGA.cb[pos2 +  7] = 0;
    VGA.cb[pos2 +  8] = 0;
    VGA.cb[pos2 +  9] = 0;
    VGA.cb[pos2 + 10] = 0;
    VGA.cb[pos2 + 11] = 0;
    VGA.cb[pos2 + 12] = 0;
    VGA.cb[pos2 + 13] = 0;
    VGA.cb[pos2 + 14] = 0;
    VGA.cb[pos2 + 15] = 0;

    pos1 += 320;
    pos2 += 320;
  }

  pos1 = 320 * posBolaY + posBolaX;
  for (i=0; i<tamBola; i++) {
    VGA.cb[pos1 +  0] = 0;
    VGA.cb[pos1 +  1] = 0;
    VGA.cb[pos1 +  2] = 0;
    VGA.cb[pos1 +  3] = 0;
    VGA.cb[pos1 +  4] = 0;
    VGA.cb[pos1 +  5] = 0;
    VGA.cb[pos1 +  6] = 0;
    VGA.cb[pos1 +  7] = 0;
    VGA.cb[pos1 +  8] = 0;
    VGA.cb[pos1 +  9] = 0;
    VGA.cb[pos1 + 10] = 0;
    VGA.cb[pos1 + 11] = 0;
    VGA.cb[pos1 + 12] = 0;
    VGA.cb[pos1 + 13] = 0;
    VGA.cb[pos1 + 14] = 0;
    VGA.cb[pos1 + 15] = 0;

    pos1 += 320;
  }
}

void dibujarCuentaRegresiva() {
  int i;
  int pos1, posD;
  byte pix;

  pos1 = 320 * 160 + 320 / 2 - 6;
  posD = cuentaRegresiva * 3 * 5;

  for (i=0; i<5; i++) {
    pix = digito[posD + 0];
    VGA.cb[pos1 +  0 + 320 * 0] = pix;
    VGA.cb[pos1 +  1 + 320 * 0] = pix;
    VGA.cb[pos1 +  2 + 320 * 0] = pix;
    VGA.cb[pos1 +  3 + 320 * 0] = pix;
    VGA.cb[pos1 +  0 + 320 * 1] = pix;
    VGA.cb[pos1 +  1 + 320 * 1] = pix;
    VGA.cb[pos1 +  2 + 320 * 1] = pix;
    VGA.cb[pos1 +  3 + 320 * 1] = pix;
    VGA.cb[pos1 +  0 + 320 * 2] = pix;
    VGA.cb[pos1 +  1 + 320 * 2] = pix;
    VGA.cb[pos1 +  2 + 320 * 2] = pix;
    VGA.cb[pos1 +  3 + 320 * 2] = pix;
    VGA.cb[pos1 +  0 + 320 * 3] = pix;
    VGA.cb[pos1 +  1 + 320 * 3] = pix;
    VGA.cb[pos1 +  2 + 320 * 3] = pix;
    VGA.cb[pos1 +  3 + 320 * 3] = pix;

    pix = digito[posD + 1];
    VGA.cb[pos1 +  4 + 320 * 0] = pix;
    VGA.cb[pos1 +  5 + 320 * 0] = pix;
    VGA.cb[pos1 +  6 + 320 * 0] = pix;
    VGA.cb[pos1 +  7 + 320 * 0] = pix;
    VGA.cb[pos1 +  4 + 320 * 1] = pix;
    VGA.cb[pos1 +  5 + 320 * 1] = pix;
    VGA.cb[pos1 +  6 + 320 * 1] = pix;
    VGA.cb[pos1 +  7 + 320 * 1] = pix;
    VGA.cb[pos1 +  4 + 320 * 2] = pix;
    VGA.cb[pos1 +  5 + 320 * 2] = pix;
    VGA.cb[pos1 +  6 + 320 * 2] = pix;
    VGA.cb[pos1 +  7 + 320 * 2] = pix;
    VGA.cb[pos1 +  4 + 320 * 3] = pix;
    VGA.cb[pos1 +  5 + 320 * 3] = pix;
    VGA.cb[pos1 +  6 + 320 * 3] = pix;
    VGA.cb[pos1 +  7 + 320 * 3] = pix;

    pix = digito[posD + 2];
    VGA.cb[pos1 +  8 + 320 * 0] = pix;
    VGA.cb[pos1 +  9 + 320 * 0] = pix;
    VGA.cb[pos1 + 10 + 320 * 0] = pix;
    VGA.cb[pos1 + 11 + 320 * 0] = pix;
    VGA.cb[pos1 +  8 + 320 * 1] = pix;
    VGA.cb[pos1 +  9 + 320 * 1] = pix;
    VGA.cb[pos1 + 10 + 320 * 1] = pix;
    VGA.cb[pos1 + 11 + 320 * 1] = pix;
    VGA.cb[pos1 +  8 + 320 * 2] = pix;
    VGA.cb[pos1 +  9 + 320 * 2] = pix;
    VGA.cb[pos1 + 10 + 320 * 2] = pix;
    VGA.cb[pos1 + 11 + 320 * 2] = pix;
    VGA.cb[pos1 +  8 + 320 * 3] = pix;
    VGA.cb[pos1 +  9 + 320 * 3] = pix;
    VGA.cb[pos1 + 10 + 320 * 3] = pix;
    VGA.cb[pos1 + 11 + 320 * 3] = pix;

    pos1 += 320 * 4;
    posD += 3;
  }
}

void borrarCuentaRegresiva() {
  int i;
  int pos1;

  pos1 = 320 * 160 + 320 / 2 - 6;

  for (i=0; i<5; i++) {
    VGA.cb[pos1 +  0 + 320 * 0] = 0;
    VGA.cb[pos1 +  1 + 320 * 0] = 0;
    VGA.cb[pos1 +  2 + 320 * 0] = 0;
    VGA.cb[pos1 +  3 + 320 * 0] = 0;
    VGA.cb[pos1 +  0 + 320 * 1] = 0;
    VGA.cb[pos1 +  1 + 320 * 1] = 0;
    VGA.cb[pos1 +  2 + 320 * 1] = 0;
    VGA.cb[pos1 +  3 + 320 * 1] = 0;
    VGA.cb[pos1 +  0 + 320 * 2] = 0;
    VGA.cb[pos1 +  1 + 320 * 2] = 0;
    VGA.cb[pos1 +  2 + 320 * 2] = 0;
    VGA.cb[pos1 +  3 + 320 * 2] = 0;
    VGA.cb[pos1 +  0 + 320 * 3] = 0;
    VGA.cb[pos1 +  1 + 320 * 3] = 0;
    VGA.cb[pos1 +  2 + 320 * 3] = 0;
    VGA.cb[pos1 +  3 + 320 * 3] = 0;

    VGA.cb[pos1 +  4 + 320 * 0] = 0;
    VGA.cb[pos1 +  5 + 320 * 0] = 0;
    VGA.cb[pos1 +  6 + 320 * 0] = 0;
    VGA.cb[pos1 +  7 + 320 * 0] = 0;
    VGA.cb[pos1 +  4 + 320 * 1] = 0;
    VGA.cb[pos1 +  5 + 320 * 1] = 0;
    VGA.cb[pos1 +  6 + 320 * 1] = 0;
    VGA.cb[pos1 +  7 + 320 * 1] = 0;
    VGA.cb[pos1 +  4 + 320 * 2] = 0;
    VGA.cb[pos1 +  5 + 320 * 2] = 0;
    VGA.cb[pos1 +  6 + 320 * 2] = 0;
    VGA.cb[pos1 +  7 + 320 * 2] = 0;
    VGA.cb[pos1 +  4 + 320 * 3] = 0;
    VGA.cb[pos1 +  5 + 320 * 3] = 0;
    VGA.cb[pos1 +  6 + 320 * 3] = 0;
    VGA.cb[pos1 +  7 + 320 * 3] = 0;

    VGA.cb[pos1 +  8 + 320 * 0] = 0;
    VGA.cb[pos1 +  9 + 320 * 0] = 0;
    VGA.cb[pos1 + 10 + 320 * 0] = 0;
    VGA.cb[pos1 + 11 + 320 * 0] = 0;
    VGA.cb[pos1 +  8 + 320 * 1] = 0;
    VGA.cb[pos1 +  9 + 320 * 1] = 0;
    VGA.cb[pos1 + 10 + 320 * 1] = 0;
    VGA.cb[pos1 + 11 + 320 * 1] = 0;
    VGA.cb[pos1 +  8 + 320 * 2] = 0;
    VGA.cb[pos1 +  9 + 320 * 2] = 0;
    VGA.cb[pos1 + 10 + 320 * 2] = 0;
    VGA.cb[pos1 + 11 + 320 * 2] = 0;
    VGA.cb[pos1 +  8 + 320 * 3] = 0;
    VGA.cb[pos1 +  9 + 320 * 3] = 0;
    VGA.cb[pos1 + 10 + 320 * 3] = 0;
    VGA.cb[pos1 + 11 + 320 * 3] = 0;

    pos1 += 320 * 4;
  }
}

void dibujarWinLose(int ganador) {
  byte pix;
  int i, j;
  int posW, posL;

  if (ganador == 0) {
    posW = 320 * 110 + 320 / 2 - 100;
    posL = 320 * 110 + 320 / 2 + 50;
  }
  else {
    posW = 320 * 110 + 320 / 2 + 50;
    posL = 320 * 110 + 320 / 2 - 100;
  }

  for (j=0; j<5; j++) {
    for (i=0; i<12; i++) {
      pix = msgWin[j*12 + i];
      VGA.cb[posW +  0 + 320 * 0] = pix;
      VGA.cb[posW +  1 + 320 * 0] = pix;
      VGA.cb[posW +  2 + 320 * 0] = pix;
      VGA.cb[posW +  3 + 320 * 0] = pix;
      VGA.cb[posW +  0 + 320 * 1] = pix;
      VGA.cb[posW +  1 + 320 * 1] = pix;
      VGA.cb[posW +  2 + 320 * 1] = pix;
      VGA.cb[posW +  3 + 320 * 1] = pix;
      VGA.cb[posW +  0 + 320 * 2] = pix;
      VGA.cb[posW +  1 + 320 * 2] = pix;
      VGA.cb[posW +  2 + 320 * 2] = pix;
      VGA.cb[posW +  3 + 320 * 2] = pix;
      VGA.cb[posW +  0 + 320 * 3] = pix;
      VGA.cb[posW +  1 + 320 * 3] = pix;
      VGA.cb[posW +  2 + 320 * 3] = pix;
      VGA.cb[posW +  3 + 320 * 3] = pix;
      posW += 4;

    }
    posW += 320*4 - 12*4;
  }

  for (j=0; j<5; j++) {
    for (i=0; i<15; i++) {
      pix = msgLose[j*15 + i];
      VGA.cb[posL +  0 + 320 * 0] = pix;
      VGA.cb[posL +  1 + 320 * 0] = pix;
      VGA.cb[posL +  2 + 320 * 0] = pix;
      VGA.cb[posL +  3 + 320 * 0] = pix;
      VGA.cb[posL +  0 + 320 * 1] = pix;
      VGA.cb[posL +  1 + 320 * 1] = pix;
      VGA.cb[posL +  2 + 320 * 1] = pix;
      VGA.cb[posL +  3 + 320 * 1] = pix;
      VGA.cb[posL +  0 + 320 * 2] = pix;
      VGA.cb[posL +  1 + 320 * 2] = pix;
      VGA.cb[posL +  2 + 320 * 2] = pix;
      VGA.cb[posL +  3 + 320 * 2] = pix;
      VGA.cb[posL +  0 + 320 * 3] = pix;
      VGA.cb[posL +  1 + 320 * 3] = pix;
      VGA.cb[posL +  2 + 320 * 3] = pix;
      VGA.cb[posL +  3 + 320 * 3] = pix;
      posL += 4;
    }
    posL += 320*4 - 15*4;
  }
}

void borrarWinLose(int ganador) {
  int i, j;
  int posW, posL;

  if (ganador == 0) {
    posW = 320 * 110 + 320 / 2 - 100;
    posL = 320 * 110 + 320 / 2 + 50;
  }
  else {
    posW = 320 * 110 + 320 / 2 + 50;
    posL = 320 * 110 + 320 / 2 - 100;
  }

  for (j=0; j<5; j++) {
    for (i=0; i<12; i++) {
      VGA.cb[posW +  0 + 320 * 0] = 0;
      VGA.cb[posW +  1 + 320 * 0] = 0;
      VGA.cb[posW +  2 + 320 * 0] = 0;
      VGA.cb[posW +  3 + 320 * 0] = 0;
      VGA.cb[posW +  0 + 320 * 1] = 0;
      VGA.cb[posW +  1 + 320 * 1] = 0;
      VGA.cb[posW +  2 + 320 * 1] = 0;
      VGA.cb[posW +  3 + 320 * 1] = 0;
      VGA.cb[posW +  0 + 320 * 2] = 0;
      VGA.cb[posW +  1 + 320 * 2] = 0;
      VGA.cb[posW +  2 + 320 * 2] = 0;
      VGA.cb[posW +  3 + 320 * 2] = 0;
      VGA.cb[posW +  0 + 320 * 3] = 0;
      VGA.cb[posW +  1 + 320 * 3] = 0;
      VGA.cb[posW +  2 + 320 * 3] = 0;
      VGA.cb[posW +  3 + 320 * 3] = 0;
      posW += 4;

    }
    posW += 320*4 - 12*4;
  }

  for (j=0; j<5; j++) {
    for (i=0; i<15; i++) {
      VGA.cb[posL +  0 + 320 * 0] = 0;
      VGA.cb[posL +  1 + 320 * 0] = 0;
      VGA.cb[posL +  2 + 320 * 0] = 0;
      VGA.cb[posL +  3 + 320 * 0] = 0;
      VGA.cb[posL +  0 + 320 * 1] = 0;
      VGA.cb[posL +  1 + 320 * 1] = 0;
      VGA.cb[posL +  2 + 320 * 1] = 0;
      VGA.cb[posL +  3 + 320 * 1] = 0;
      VGA.cb[posL +  0 + 320 * 2] = 0;
      VGA.cb[posL +  1 + 320 * 2] = 0;
      VGA.cb[posL +  2 + 320 * 2] = 0;
      VGA.cb[posL +  3 + 320 * 2] = 0;
      VGA.cb[posL +  0 + 320 * 3] = 0;
      VGA.cb[posL +  1 + 320 * 3] = 0;
      VGA.cb[posL +  2 + 320 * 3] = 0;
      VGA.cb[posL +  3 + 320 * 3] = 0;
      posL += 4;
    }
    posL += 320*4 - 15*4;
  }
}
